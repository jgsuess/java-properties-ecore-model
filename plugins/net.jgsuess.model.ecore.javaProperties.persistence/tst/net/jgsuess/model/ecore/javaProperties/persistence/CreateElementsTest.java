/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
package net.jgsuess.model.ecore.javaProperties.persistence;

import static java.util.logging.LogManager.getLogManager;
import static net.jgsuess.model.ecore.javaProperties.persistence.internal.TranscriptionUtil.loadFromJavaPropertiesInputStream;
import static net.jgsuess.model.ecore.javaProperties.persistence.internal.TranscriptionUtil.saveToJavaPropertiesOutputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.jgsuess.model.ecore.javaProperties.Properties;

public class CreateElementsTest {

	@BeforeEach
	void configureLogger() throws IOException {
		getLogManager().readConfiguration(getClass().getResourceAsStream("logging.properties"));
	}

	@Test
	void test2() throws IOException {

		final OutputStream outputStream = new FileOutputStream("out.properties");
		final String comments = "";

		final Properties fromJavaPropertiesInputStream = loadFromJavaPropertiesInputStream(
				getClass().getResourceAsStream("logging.properties"));
		saveToJavaPropertiesOutputStream(fromJavaPropertiesInputStream, outputStream, comments);

	}

}
