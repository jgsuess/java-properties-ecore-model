/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
package net.jgsuess.model.ecore.javaProperties.persistence.internal;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toUnmodifiableList;
import static net.jgsuess.model.ecore.javaProperties.persistence.internal.IOUtil.loadMapFromPropertyInputStream;
import static net.jgsuess.model.ecore.javaProperties.persistence.internal.IOUtil.saveMapToPropertyOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EMap;

import lombok.extern.java.Log;
import net.jgsuess.model.ecore.javaProperties.JavaPropertiesFactory;
import net.jgsuess.model.ecore.javaProperties.Node;
import net.jgsuess.model.ecore.javaProperties.Properties;
import net.jgsuess.model.ecore.javaProperties.util.JavaPropertiesSwitch;

@Log
public class TranscriptionUtil {

	public static final class TranscriptionWalker extends JavaPropertiesSwitch<Boolean> {
		private final Map<String, String> map;
		final Stack<String> stack = new Stack<>();

		public TranscriptionWalker(final Map<String, String> map) {
			this.map = map;
		}

		@Override
		public Boolean caseProperties(final Properties properties) {
			return properties.getNodes().stream().allMatch(this::caseNameToNode);
		}

		@Override
		public Boolean caseNameToNode(final Entry<String, Node> entry) {

			/*
			 * Push the current name segment onto the stack. This will be used inside the
			 * node to render the complete property by assembling all segments on the stack.
			 */
			final String segment = entry.getKey();
			stack.push(segment);

			/*
			 * Descend into the node. This will may render the property name or push further
			 * segments on the stack by recursion.
			 */
			final Node node = entry.getValue();
			final Boolean allProcessedOk = doSwitch(node);

			/*
			 * After processing of the node is finished, the segment is removed.
			 */
			stack.pop();

			return allProcessedOk;
		}

		@Override
		public Boolean caseNode(final Node node) {

			/*
			 * Descend into nodes, checking processing was ok for all nodes.
			 */
			final boolean allProcessedOk = node.getNodes().stream().allMatch(this::caseNameToNode);

			/*
			 * If the node in the model has a property set, add a corresponding property to
			 * the output.
			 */
			final String value = node.getValue();

			if (value != null) {
				/*
				 * The name is the current value recorded on the stack, separated by dots.
				 */
				final String propertyName = stack.stream().collect(joining("."));
				/*
				 * add the propertyName and value to the output Java properties
				 */
				map.put(propertyName, value);
			}

			/*
			 * Signal the node was processed ok.
			 */
			return allProcessedOk;
		}
	}

	public static final String LITERAL_DOT = "\\.";
	public static final JavaPropertiesFactory FACTORY = JavaPropertiesFactory.eINSTANCE;

	/**
	 * Merge the new storage node with the existing node. Get the new nodes' map.
	 * Each entry of that map needs to be merged. If the key exists, place the
	 * value, otherwise merge the new node with the existing node.
	 * 
	 * @param nodeToMerge
	 * @param containerNode
	 * @return
	 */
	public static final Node mergeIntoContainerNode(final Node nodeToMerge, final Node containerNode) {

		final Map<String, Node> newNodesMap = nodeToMerge.getNodes().map();

		/*
		 * Merge each entry from the new map with the existing map if the key does not
		 * exist, use the value from the new node. if the key does exist merge the nodes
		 */
		newNodesMap.forEach((k, v) -> containerNode.getNodes().map().merge(k, v, (n1, n2) -> mergeNodes(n1, n2)));
		return containerNode;
	}

	public static final Node mergeNodes(final Node n1, final Node n2) {

		final List<Node> nodesToMerge = asList(n1, n2);

		final List<String> values = nodesToMerge.stream().map(Node::getValue).map(Optional::ofNullable)
				.filter(Optional::isPresent).map(Optional::get).collect(toUnmodifiableList());
		final int numberOfValues = values.size();

		assert numberOfValues <= 1;

		if (numberOfValues == 1) {
			n2.setValue(values.get(0));
		}

		return mergeIntoContainerNode(n1, n2);
	}

	public static final Properties loadFromJavaPropertiesInputStream(final InputStream inputStream) throws IOException {

		final Map<String, String> map = loadMapFromPropertyInputStream(inputStream);

		/*
		 * Convert the map to a node
		 */
		final Node node = fromMap(map);

		/*
		 * Create the model properties
		 */
		final Properties modelProperties = FACTORY.createProperties();

		/*
		 * Assign the nodes' node map from the model properties
		 */
		modelProperties.getNodes().addAll(node.getNodes());

		/*
		 * Return the model properties
		 */
		return modelProperties;

	}

	public static final Node fromMap(final Map<String, String> map) {

		/*
		 * Create an empty Node to serve as the container
		 */

		final Node containerNode = FACTORY.createNode();

		/*
		 * For each entry, create a Node from Entry fromEntry()
		 */

		final Stream<Node> nodesToMerge = map.entrySet().stream().map(TranscriptionUtil::fromEntry);

		/*
		 * Create a consumer that adds merge nodes to the container
		 */

		final Consumer<Node> mergeIntoContainer = nodeToMerge -> mergeIntoContainerNode(nodeToMerge, containerNode);

		/*
		 * Apply the consumer to merge all newly created nodes
		 */
		nodesToMerge.peek(node -> log.fine(printValue(node))).forEach(mergeIntoContainer);

		/*
		 * Return the container node
		 */
		return containerNode;
	}

	public static final String printValue(final Node node) {
		final EMap<String, Node> nodes = node.getNodes();
		final int size = nodes.size();
		assert size <= 1;

		if (nodes.isEmpty()) {
			return node.getValue();
		} else {
			final Entry<String, Node> entry = nodes.get(0);
			return entry.getKey() + "." + printValue(entry.getValue());
		}

	}

	public static final Node fromEntry(final Entry<String, String> entry) {

		final List<String> segments = toSegments(entry.getKey());
		final String value = entry.getValue();

		return fromEntry(segments, value);
	}

	/**
	 * Break the property name into segments at dot characters and trim the
	 * resulting segments.
	 * 
	 * @param key
	 * @return
	 */
	public static final List<String> toSegments(final String key) {
		return stream(key.split(LITERAL_DOT)).map(String::trim).collect(toUnmodifiableList());
	}

	/**
	 * Create a node with a chain of nested nodes from the segment. Set the value on
	 * the last node. The value may not be null.
	 * 
	 * @param segments
	 * @param value
	 * @return
	 */
	public static final Node fromEntry(final List<String> segments, final String value) {

		requireNonNull(value);

		final Node node = FACTORY.createNode();

		if (segments.isEmpty()) {
			node.setValue(value);
		} else {
			final String head = segments.get(0);
			final List<String> tail = segments.subList(1, segments.size());
			node.getNodes().put(head, fromEntry(tail, value));
		}

		return node;
	}

	public static final void saveToJavaPropertiesOutputStream(final Properties properties, final OutputStream outputStream,
			final String comments) throws IOException {

		Map<String, String> map = new HashMap<>();

		final JavaPropertiesSwitch<Boolean> transcriptionWalker = new TranscriptionWalker(map);

		transcriptionWalker.doSwitch(properties);

		saveMapToPropertyOutputStream(map, outputStream, comments);

	}
}
