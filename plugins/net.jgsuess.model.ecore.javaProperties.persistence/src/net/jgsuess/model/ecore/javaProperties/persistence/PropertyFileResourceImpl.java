/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
package net.jgsuess.model.ecore.javaProperties.persistence;

import static net.jgsuess.model.ecore.javaProperties.persistence.internal.TranscriptionUtil.loadFromJavaPropertiesInputStream;
import static net.jgsuess.model.ecore.javaProperties.persistence.internal.TranscriptionUtil.saveToJavaPropertiesOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;

import lombok.extern.java.Log;
import net.jgsuess.model.ecore.javaProperties.Properties;



@Log
public class PropertyFileResourceImpl extends ResourceImpl {
	
	public static final String DEFAULT_PROPERTY_SAVE_COMMENT = "";
	public static final String OPTION_PROPERTY_SAVE_COMMENT = "PROPERTY_SAVE_COMMENT";
	
	public PropertyFileResourceImpl(URI uri) {
		super(uri);
	}

	@Override
	protected void doLoad(final InputStream inputStream, final Map<?, ?> options) throws IOException {
		getContents().add(loadFromJavaPropertiesInputStream(inputStream));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doSave(final OutputStream outputStream, final Map<?, ?> options) throws IOException {
		/*
		 * Check that there is only one element in this resource
		 */
		if (getContents().size() > 1) {
			throw new IllegalArgumentException("Resource contains more than one content.");
		}

		/*
		 * Get the contents of the resource. This can fail.
		 */
		final EObject eObject = getContents().get(0);

		if (eObject == null) {
			log.info("Resource is empty. Nothing to do.");
		} else {
			/*
			 * Cast the object to model properties. May throw a class cast.
			 */
			final Properties modelProperties = (Properties) eObject;

			String comment = getComment(options);
			
			/*
			 * Cast the map to the most general type. This is safe.
			 */
			
			/*
			 * Store the model properties in a Java properties output stream
			 */
			saveToJavaPropertiesOutputStream(modelProperties, outputStream, comment);
		}

	}

	private String getComment(final Map<?, ?> options) {
		String comment = "";

		if(options != null) {
			
			final Map<Object, Object> map = (Map<Object,Object>) options;
			
			/*
			 * Get the value of the save property
			 */
			final Object propertySaveValue = map.getOrDefault(OPTION_PROPERTY_SAVE_COMMENT, DEFAULT_PROPERTY_SAVE_COMMENT);
			
			/*
			 * Cast it to a String. This can fail.
			 */
			comment = (String) propertySaveValue;
		}
		return comment;
	}

}
