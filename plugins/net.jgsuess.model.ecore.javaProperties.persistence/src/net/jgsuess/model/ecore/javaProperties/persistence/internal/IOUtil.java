/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
package net.jgsuess.model.ecore.javaProperties.persistence.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;

public class IOUtil {

	public static final Map<String, String> loadMapFromPropertyInputStream(final InputStream inputStream)
			throws IOException {
		
		/*
		 * Create properties for storage
		 */
		final Properties properties = new Properties();

		/*
		 * Parse properties, make sure they are sound
		 */
		properties.load(inputStream);

		/*
		 * Get the underlying map of the properties
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, String> map = (Map) properties;
		
		/*
		 * Return the map
		 */
		return map;
	}

	public static final void saveMapToPropertyOutputStream(final Map<String, String> map,
			final OutputStream outputStream, final String comments) throws IOException {
		
		/*
		 * Create properties for storage
		 */
		final Properties properties = new Properties();
		
		/*
		 * Add the map contents to the properties
		 */
		properties.putAll(map);
		
		/*
		 * Write the properties stream to output
		 */
		properties.store(outputStream, comments);

	}

}
