/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
/**
 */
package net.jgsuess.model.ecore.javaProperties;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.model.ecore.javaProperties.JavaPropertiesFactory
 * @model kind="package"
 * @generated
 */
public interface JavaPropertiesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "javaProperties";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/model/ecore/javaProperties/0.0.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "p";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	JavaPropertiesPackage eINSTANCE = net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.model.ecore.javaProperties.impl.NameToNodeImpl <em>Name To Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see net.jgsuess.model.ecore.javaProperties.impl.NameToNodeImpl
	 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getNameToNode()
	 * @generated
	 */
	int NAME_TO_NODE = 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_TO_NODE__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_TO_NODE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Name To Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 * @ordered
	 */
	int NAME_TO_NODE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link net.jgsuess.model.ecore.javaProperties.impl.PropertiesImpl <em>Properties</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see net.jgsuess.model.ecore.javaProperties.impl.PropertiesImpl
	 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getProperties()
	 * @generated
	 */
	int PROPERTIES = 1;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES__NODES = 0;

	/**
	 * The number of structural features of the '<em>Properties</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.model.ecore.javaProperties.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see net.jgsuess.model.ecore.javaProperties.impl.NodeImpl
	 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 2;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NODES = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Node</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Name To Node</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name To Node</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueType="net.jgsuess.model.ecore.javaProperties.Node" valueContainment="true"
	 *        annotation="exeed classIcon='package' referenceLabel='return self.key;' label='return self.key;'"
	 * @generated
	 */
	EClass getNameToNode();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNameToNode()
	 * @generated
	 */
	EAttribute getNameToNode_Key();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getNameToNode()
	 * @generated
	 */
	EReference getNameToNode_Value();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.model.ecore.javaProperties.Properties <em>Properties</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>Properties</em>'.
	 * @see net.jgsuess.model.ecore.javaProperties.Properties
	 * @generated
	 */
	EClass getProperties();

	/**
	 * Returns the meta object for the map '{@link net.jgsuess.model.ecore.javaProperties.Properties#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Nodes</em>'.
	 * @see net.jgsuess.model.ecore.javaProperties.Properties#getNodes()
	 * @see #getProperties()
	 * @generated
	 */
	EReference getProperties_Nodes();

	/**
	 * Returns the meta object for class
	 * '{@link net.jgsuess.model.ecore.javaProperties.Node <em>Node</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the meta object for class '<em>Node</em>'.
	 * @see net.jgsuess.model.ecore.javaProperties.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the map '{@link net.jgsuess.model.ecore.javaProperties.Node#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Nodes</em>'.
	 * @see net.jgsuess.model.ecore.javaProperties.Node#getNodes()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Nodes();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.model.ecore.javaProperties.Node#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see net.jgsuess.model.ecore.javaProperties.Node#getValue()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Value();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JavaPropertiesFactory getJavaPropertiesFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.model.ecore.javaProperties.impl.NameToNodeImpl <em>Name To Node</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see net.jgsuess.model.ecore.javaProperties.impl.NameToNodeImpl
		 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getNameToNode()
		 * @generated
		 */
		EClass NAME_TO_NODE = eINSTANCE.getNameToNode();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 *
		 * @generated
		 */
		EAttribute NAME_TO_NODE__KEY = eINSTANCE.getNameToNode_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_TO_NODE__VALUE = eINSTANCE.getNameToNode_Value();

		/**
		 * The meta object literal for the '{@link net.jgsuess.model.ecore.javaProperties.impl.PropertiesImpl <em>Properties</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see net.jgsuess.model.ecore.javaProperties.impl.PropertiesImpl
		 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getProperties()
		 * @generated
		 */
		EClass PROPERTIES = eINSTANCE.getProperties();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTIES__NODES = eINSTANCE.getProperties_Nodes();

		/**
		 * The meta object literal for the '{@link net.jgsuess.model.ecore.javaProperties.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see net.jgsuess.model.ecore.javaProperties.impl.NodeImpl
		 * @see net.jgsuess.model.ecore.javaProperties.impl.JavaPropertiesPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' map feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 *
		 * @generated
		 */
		EReference NODE__NODES = eINSTANCE.getNode_Nodes();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__VALUE = eINSTANCE.getNode_Value();

	}

} // JavaPropertiesPackage
