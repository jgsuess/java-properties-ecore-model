/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
/**
 */
package net.jgsuess.model.ecore.javaProperties.impl;

import java.util.Map;

import net.jgsuess.model.ecore.javaProperties.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import net.jgsuess.model.ecore.javaProperties.JavaPropertiesFactory;
import net.jgsuess.model.ecore.javaProperties.JavaPropertiesPackage;
import net.jgsuess.model.ecore.javaProperties.Node;
import net.jgsuess.model.ecore.javaProperties.Properties;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class JavaPropertiesFactoryImpl extends EFactoryImpl implements JavaPropertiesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static JavaPropertiesFactory init() {
		try {
			JavaPropertiesFactory theJavaPropertiesFactory = (JavaPropertiesFactory)EPackage.Registry.INSTANCE.getEFactory(JavaPropertiesPackage.eNS_URI);
			if (theJavaPropertiesFactory != null) {
				return theJavaPropertiesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JavaPropertiesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 *
	 * @generated
	 */
	public JavaPropertiesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JavaPropertiesPackage.NAME_TO_NODE: return (EObject)createNameToNode();
			case JavaPropertiesPackage.PROPERTIES: return createProperties();
			case JavaPropertiesPackage.NODE: return createNode();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, Node> createNameToNode() {
		NameToNodeImpl nameToNode = new NameToNodeImpl();
		return nameToNode;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Properties createProperties() {
		PropertiesImpl properties = new PropertiesImpl();
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node createNode() {
		NodeImpl node = new NodeImpl();
		return node;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public JavaPropertiesPackage getJavaPropertiesPackage() {
		return (JavaPropertiesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JavaPropertiesPackage getPackage() {
		return JavaPropertiesPackage.eINSTANCE;
	}

} // JavaPropertiesFactoryImpl
