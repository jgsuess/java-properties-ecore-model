/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * This program and the accompanying materials are made available under
 *  the  terms of the Eclipse Public License 2.0 which is available at
 *  http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 3
 *
 * Contributors:
 *     Jörn Guy Süß - initial API and implementation
 *******************************************************************************/
/**
 */
package net.jgsuess.model.ecore.javaProperties;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Node</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.jgsuess.model.ecore.javaProperties.Node#getNodes <em>Nodes</em>}</li>
 *   <li>{@link net.jgsuess.model.ecore.javaProperties.Node#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see net.jgsuess.model.ecore.javaProperties.JavaPropertiesPackage#getNode()
 * @model annotation="exeed classIcon='set' label='return \'Value: \' + self.value;'"
 * @generated
 */
public interface Node extends EObject {

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link net.jgsuess.model.ecore.javaProperties.Node},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' map.
	 * @see net.jgsuess.model.ecore.javaProperties.JavaPropertiesPackage#getNode_Nodes()
	 * @model mapType="net.jgsuess.model.ecore.javaProperties.NameToNode&lt;org.eclipse.emf.ecore.EString, net.jgsuess.model.ecore.javaProperties.Node&gt;"
	 * @generated
	 */
	EMap<String, Node> getNodes();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see net.jgsuess.model.ecore.javaProperties.JavaPropertiesPackage#getNode_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link net.jgsuess.model.ecore.javaProperties.Node#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);
} // Node
