/*******************************************************************************
 * Copyright (c) 2022 Jörn Guy Süß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.jgsuess.model.ecore.javaProperties.persistence.tests;

import static java.util.Collections.EMPTY_MAP;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.StreamSupport.stream;
import static org.eclipse.emf.common.util.URI.createFileURI;
import static org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.jupiter.api.Test;

import net.jgsuess.model.ecore.javaProperties.persistence.PropertyFileResourceFactoryImpl;

class ResourceTest {

	@Test
	void resourceFactoryIsRegisteredTest() {
		final URI fileUri = createFileURI("example.properties");
		final Factory factory = createFactory(fileUri);
		assertNotNull(factory);

		final Class<?> expectedType = PropertyFileResourceFactoryImpl.class;
		assertInstanceOf(expectedType, factory);
	}

	@Test
	void resourceLoadsSuccesFully() throws IOException {
		final String pathName = "in.properties";
		final URI inPropertiesFileUri = createFileURI(pathName);

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource propertyFileResource = resourceSet.getResource(inPropertiesFileUri, true);

		propertyFileResource.load(EMPTY_MAP);

		final EObject root = propertyFileResource.getContents().get(0);
		assertNotNull(root);

		final EList<EObject> eContents = root.eContents();

		assertEquals(eContents.size(), 4);

		final Iterator<EObject> iterator = root.eAllContents();

		final long count = stream(spliteratorUnknownSize(iterator, ORDERED), false).count();
		
		assertEquals(count, 40);

		final URI xmiFileUri = createFileURI(pathName + ".xmi");

		final Resource xmiFileResource = resourceSet.createResource(xmiFileUri);

		xmiFileResource.getContents().add(root);
		xmiFileResource.save(EMPTY_MAP);

		final URI outPropertiesFileUri = createFileURI("out.properties");

		final Resource outPropertiesFileResource = resourceSet.createResource(outPropertiesFileUri);

		outPropertiesFileResource.getContents().add(root);

		outPropertiesFileResource.save(EMPTY_MAP);
		
		final Properties inProperties = new Properties();
		inProperties.load(new FileReader(inPropertiesFileUri.toFileString()));
		final Properties outProperties = new Properties();
		outProperties.load(new FileReader(outPropertiesFileUri.toFileString()));
		
		assertEquals(inProperties, outProperties);

	}

	private static final Factory createFactory(URI uri) {
		return INSTANCE.getFactory(uri);
	}

}
